#pragma once
#include <cinttypes>

namespace lib {
	namespace sps {
		struct isps {
			virtual int getWidth() const = 0;
			virtual int getHeight() const = 0;
			virtual float getFrameRate() const = 0;
			virtual int getFormat() const = 0;
			virtual const char* getCodecName() const = 0;
			virtual const char* getProfileName() const = 0;
			virtual uint8_t getProfileIdc() const = 0;
			virtual const char* getColorFormatName() const = 0;
			virtual const char* getLevelName() const = 0;
			virtual uint8_t getLevelIdc() const = 0;
		};
	}
}
