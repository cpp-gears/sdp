#include "h265sps.h"
#include "cbitstream.h"

using namespace lib::sps;

h265::h265(const uint8_t* sps_data, std::size_t sps_length) {
	if (sps_data && sps_length) {
		cbitstream s(sps_data, sps_length);
		// skipping header
		s.skipBits(4);
		spsVideoParameterSetId = (uint8_t)s.getBits(4);
		spsMaxSubLayersMinus1 = (uint8_t)s.getBits(3);
		spsTemporalIdNestingFlag = (uint8_t)s.getBits(1);


		// parsing profile tier level
		generalProfileSpace = (uint8_t)s.getBits(2);
		generalTierFlag = (uint8_t)s.getBits(1);
		generalProfileIdc = (uint8_t)s.getBits(5);

		for (size_t i = 0; i < 32; i++)
		{
			generalProfileCompatibilityFlag[i] = (uint8_t)s.getBits(1);
		}

		generalProgressiveSourceFlag = (uint8_t)s.getBits(1);
		generalInterlacedSourceFlag = (uint8_t)s.getBits(1);
		generalNonPackedConstraintFlag = (uint8_t)s.getBits(1);
		generalFrameOnlyConstraintFlag = (uint8_t)s.getBits(1);

		// Reserved zero bits
		s.skipBits(44);

		generalLevelIdc = (uint8_t)s.getBits(8);

		subLayerProfilePresentFlag.resize(spsMaxSubLayersMinus1);
		subLayerLevelPresentFlag.resize(spsMaxSubLayersMinus1);

		for (size_t i = 0; i < spsMaxSubLayersMinus1; i++)
		{
			subLayerProfilePresentFlag[i] = (uint8_t)s.getBits(1);
			subLayerLevelPresentFlag[i] = (uint8_t)s.getBits(1);
		}

		if (spsMaxSubLayersMinus1 > 0)
		{
			for (size_t i = spsMaxSubLayersMinus1; i < 8; i++)
			{
				s.skipBits(2);
			}
		}

		subLayerProfileSpace.resize(spsMaxSubLayersMinus1);
		subLayerTierFlag.resize(spsMaxSubLayersMinus1);
		subLayerProfileIdc.resize(spsMaxSubLayersMinus1);
		subLayerProfileCompatibilityFlag.resize(spsMaxSubLayersMinus1);
		subLayerProgressiveSourceFlag.resize(spsMaxSubLayersMinus1);
		subLayerInterlacedSourceFlag.resize(spsMaxSubLayersMinus1);
		subLayerNonPackedConstraintFlag.resize(spsMaxSubLayersMinus1);
		subLayerFrameOnlyConstraintFlag.resize(spsMaxSubLayersMinus1);
		subLayerLevelIdc.resize(spsMaxSubLayersMinus1);

		for (size_t i = 0; i < spsMaxSubLayersMinus1; i++)
		{
			if (subLayerProfilePresentFlag[i])
			{
				subLayerProfileSpace[i] = (uint8_t)s.getBits(2);
				subLayerTierFlag[i] = (uint8_t)s.getBits(1);
				subLayerProfileIdc[i] = (uint8_t)s.getBits(5);

				subLayerProfileCompatibilityFlag[i].resize(32);

				for (size_t j = 0; j < 32; j++)
				{
					subLayerProfileCompatibilityFlag[i][j] = (uint8_t)s.getBits(1);
				}

				subLayerProgressiveSourceFlag[i] = (uint8_t)s.getBits(1);
				subLayerInterlacedSourceFlag[i] = (uint8_t)s.getBits(1);
				subLayerNonPackedConstraintFlag[i] = (uint8_t)s.getBits(1);
				subLayerFrameOnlyConstraintFlag[i] = (uint8_t)s.getBits(1);

				// Reserved zero bits
				s.skipBits(44);
			}

			if (subLayerLevelPresentFlag[i])
			{
				subLayerLevelIdc[i] = (uint8_t)s.getBits(8);
			}
			else
			{
				subLayerLevelIdc[i] = 1;
			}
		}

		spsSeqParameterSetId = s.getUE();
		chromaFormatIdc = s.getUE();

		if (chromaFormatIdc == 3)
		{
			separateColourPlaneFlag = (uint8_t)s.getBits(1);
		}
		else
		{
			separateColourPlaneFlag = 0;
		}

		picWidthInLumaSamples = s.getUE();
		picHeightInLumaSamples = s.getUE();

		conformanceWindowFlag = (uint8_t)s.getBits(1);

		if (conformanceWindowFlag)
		{
			confWinLeftOffset = s.getUE();
			confWinRightOffset = s.getUE();
			confWinTopOffset = s.getUE();
			confWinBottomOffset = s.getUE();
		}

		bitDepthLumaMinus8 = s.getUE();

		bitDepthChromaMinus8 = s.getUE();
	}
}

int h265::getWidth() const { return picWidthInLumaSamples - (confWinLeftOffset + confWinRightOffset); }
int h265::getHeight() const { return picHeightInLumaSamples - (confWinTopOffset + confWinRightOffset); }
float h265::getFrameRate() const { return 0.0; }
const char* h265::getProfileName() const { return ""; }
const char* h265::getColorFormatName() const { return ""; }
const char* h265::getLevelName() const { return ""; }
