#include "h264sps.h"
#include "cbitstream.h"

using namespace lib::sps;

h264::h264(const uint8_t* sps_data, size_t sps_length) {
	if (sps_data && sps_length) {
		cbitstream s(sps_data, sps_length);
		// skipping header
		profile_idc = s.getBits(8);
		constraint_set0_flag = s.getBits();
		constraint_set1_flag = s.getBits();
		constraint_set2_flag = s.getBits();
		constraint_set3_flag = s.getBits();
		constraint_set4_flag = s.getBits();
		constraint_set5_flag = s.getBits();
		reserved_zero_2bits = s.getBits(2);
		level_idc = s.getBits(8);
		seq_parameter_set_id = s.getExponentialGolombCode();

		if (profile_idc == 100 || profile_idc == 110 ||
			profile_idc == 122 || profile_idc == 244 ||
			profile_idc == 44 || profile_idc == 83 ||
			profile_idc == 86 || profile_idc == 118 || profile_idc == 128 ||
			profile_idc == 138 || profile_idc == 139 || profile_idc == 134)
		{
			chroma_format_idc = s.getExponentialGolombCode();

			if (chroma_format_idc == 3)
			{
				residual_colour_transform_flag = s.getBits();
			}
			bit_depth_luma_minus8 = s.getExponentialGolombCode();
			bit_depth_chroma_minus8 = s.getExponentialGolombCode();
			qpprime_y_zero_transform_bypass_flag = s.getBits();
			seq_scaling_matrix_present_flag = s.getBits();

			if (seq_scaling_matrix_present_flag)
			{
				int i = 0;
				for (i = 0; i < 8; i++)
				{
					seq_scaling_list_present_flag = s.getBits();
					if (seq_scaling_list_present_flag)
					{
						int sizeOfScalingList = (i < 6) ? 16 : 64;
						int lastScale = 8;
						int nextScale = 8;
						int j = 0;
						for (j = 0; j < sizeOfScalingList; j++)
						{
							if (nextScale != 0)
							{
								delta_scale = s.getSE();
								nextScale = (lastScale + delta_scale + 256) % 256;
							}
							lastScale = (nextScale == 0) ? lastScale : nextScale;
						}
					}
				}
			}
		}

		log2_max_frame_num_minus4 = s.getExponentialGolombCode();
		pic_order_cnt_type = s.getExponentialGolombCode();
		if (pic_order_cnt_type == 0)
		{
			log2_max_pic_order_cnt_lsb_minus4 = s.getExponentialGolombCode();
		}
		else if (pic_order_cnt_type == 1)
		{
			delta_pic_order_always_zero_flag = s.getBits();
			offset_for_non_ref_pic = s.getSE();
			offset_for_top_to_bottom_field = s.getSE();
			num_ref_frames_in_pic_order_cnt_cycle = s.getExponentialGolombCode();
			int i;
			for (i = 0; i < num_ref_frames_in_pic_order_cnt_cycle; i++)
			{
				s.getSE();
				//sps->offset_for_ref_frame[ i ] = ReadSE();
			}
		}
		max_num_ref_frames = s.getExponentialGolombCode();
		gaps_in_frame_num_value_allowed_flag = s.getBits();
		pic_width_in_mbs_minus1 = s.getExponentialGolombCode();
		pic_height_in_map_units_minus1 = s.getExponentialGolombCode();
		frame_mbs_only_flag = s.getBits();
		if (!frame_mbs_only_flag)
		{
			mb_adaptive_frame_field_flag = s.getBits();
		}
		direct_8x8_inference_flag = s.getBits();
		frame_cropping_flag = s.getBits();
		if (frame_cropping_flag)
		{
			frame_crop_left_offset = s.getExponentialGolombCode();
			frame_crop_right_offset = s.getExponentialGolombCode();
			frame_crop_top_offset = s.getExponentialGolombCode();
			frame_crop_bottom_offset = s.getExponentialGolombCode();
		}
		vui_parameters_present_flag = s.getBits();

		return;

		if (vui_parameters_present_flag) {
			vui.aspect_ratio_info_present_flag = s.getBits();
			if (vui.aspect_ratio_info_present_flag)
			{
				vui.aspect_ratio_idc = s.getBits(8);
				if (vui.aspect_ratio_idc == 255 /* Extended_SAR  */)
				{
					vui.sar_width = s.getBits(16);
					vui.sar_height = s.getBits(16);
				}
			}

			vui.overscan_info_present_flag = s.getBits();
			if (vui.overscan_info_present_flag)
			{
				vui.overscan_appropriate_flag = s.getBits();
			}

			vui.video_signal_type_present_flag = s.getBits();
			if (vui.video_signal_type_present_flag)
			{
				vui.video_format = s.getBits(3);
				vui.video_full_range_flag = s.getBits();

				vui.colour_description_present_flag = s.getBits();
				if (vui.colour_description_present_flag)
				{
					vui.colour_primaries = s.getBits(8);
					vui.transfer_characteristics = s.getBits(8);
					vui.matrix_coefficients = s.getBits(8);
				}
			}

			vui.chroma_loc_info_present_flag = s.getBits();
			if (vui.chroma_loc_info_present_flag)
			{
				vui.chroma_sample_loc_type_top_field = s.getUE();
				vui.chroma_sample_loc_type_bottom_field = s.getUE();
			}

			vui.timing_info_present_flag = s.getBits();
			if (vui.timing_info_present_flag)
			{
				vui.num_units_in_tick = s.getBits(32);
				vui.time_scale = s.getBits(32);
				vui.fixed_frame_rate_flag = s.getBits();
			}

			vui.nal_hrd_parameters_present_flag = s.getBits();
			if (vui.nal_hrd_parameters_present_flag)
			{
				vui.cpb_cnt_minus1 = s.getUE();
				vui.bit_rate_scale = s.getBits(4);
				vui.cpb_size_scale = s.getBits(4);

				for (int SchedSelIdx = 0; SchedSelIdx <= vui.cpb_cnt_minus1; SchedSelIdx++)
				{
					vui.bit_rate_value_minus1[SchedSelIdx] = s.getUE();
					vui.cpb_size_value_minus1[SchedSelIdx] = s.getUE();
					vui.cbr_flag[SchedSelIdx] = s.getBits();
				}

				vui.initial_cpb_removal_delay_length_minus1 = s.getBits(5);
				vui.cpb_removal_delay_length_minus1 = s.getBits(5);
				vui.dpb_output_delay_length_minus1 = s.getBits(5);
				vui.time_offset_length = s.getBits(5);
			}


			vui.vcl_hrd_parameters_present_flag = s.getBits();
			if (vui.vcl_hrd_parameters_present_flag)
			{
				vui.cpb_cnt_minus1 = s.getUE();
				vui.bit_rate_scale = s.getBits(4);
				vui.cpb_size_scale = s.getBits(4);

				for (int SchedSelIdx = 0; SchedSelIdx <= vui.cpb_cnt_minus1; SchedSelIdx++)
				{
					vui.bit_rate_value_minus1[SchedSelIdx] = s.getUE();
					vui.cpb_size_value_minus1[SchedSelIdx] = s.getUE();
					vui.cbr_flag[SchedSelIdx] = s.getBits();
				}
				vui.initial_cpb_removal_delay_length_minus1 = s.getBits(5);
				vui.cpb_removal_delay_length_minus1 = s.getBits(5);
				vui.dpb_output_delay_length_minus1 = s.getBits(5);
				vui.time_offset_length = s.getBits(5);
			}

			if (vui.nal_hrd_parameters_present_flag \
				|| vui.vcl_hrd_parameters_present_flag)
			{
				vui.low_delay_hrd_flag = s.getBits();
			}

			vui.pic_struct_present_flag = s.getBits();

			vui.bitstream_restriction_flag = s.getBits();
			if (vui.bitstream_restriction_flag)
			{
				vui.motion_vectors_over_pic_boundaries_flag = s.getBits();
				vui.max_bytes_per_pic_denom = s.getUE();
				vui.max_bits_per_mb_denom = s.getUE();
				vui.log2_max_mv_length_horizontal = s.getUE();
				vui.log2_max_mv_length_vertical = s.getUE();
				vui.num_reorder_frames = s.getUE();
				vui.max_dec_frame_buffering = s.getUE();
			}
		}
	}
}


int h264::getWidth() const {
	return pic_width_in_mbs_minus1 ? (((pic_width_in_mbs_minus1 + 1) * 16) - frame_crop_right_offset * 2 - frame_crop_left_offset * 2) : 0;
}

int h264::getHeight() const {
	return pic_height_in_map_units_minus1 ? ((2 - frame_mbs_only_flag) * (pic_height_in_map_units_minus1 + 1) * 16) - (frame_crop_bottom_offset * 2) - (frame_crop_top_offset * 2) : 0;
}

const char* h264::getProfileName() const {
	switch (profile_idc) {
	case 66:
		return constraint_set0_flag ? "CBP" : "Baseline";
	case 88:
		return "Extended";
	case 77:
		return "Main";
	case 100:
		return constraint_set4_flag && constraint_set5_flag ? "CHiP" : (constraint_set4_flag ? "PHiP" : "HiP");
	case 110:
		return "Hi10P";
	case 122:
		return "Hi422P";
	case 244:
		return "Hi444PP";
	case 44:
		return "CAVLC";
	case 83:
		return "SBP"; // Scalable Baseline Profile (83)
	case 86:
		return "SHiP"; // Scalable High Profile (86)
	case 128:
		return "Stereo High"; // Stereo High Profile
	case 118:
		return "Multiview High"; // Stereo High Profile
	case 134:
		return "MFC High"; // Stereo High Profile
	case 135:
		return "MFC Depth High"; // Stereo High Profile
	case 138:
		return "Multiview Depth High"; // Stereo High Profile
	case 139:
		return "Enhanced Multiview Depth High"; // Stereo High Profile
	default:
		break;
	}
	return "UNKNOWN";
}

const char* h264::getColorFormatName() const {
	switch (profile_idc) {
	case 66: case 77: case 88: case 100:
		return "YUV420";
	default:
		switch (chroma_format_idc) {
		case 0: return "YUV400";
		case 1: return "YUV420";
		case 2: return "YUV422";
		case 3: return "YUV444";
		}
	}
	return "UNKNOWN";
}

const char* h264::getLevelName() const {
	switch (level_idc) {
	case 10: return "1";
	case 11: return constraint_set3_flag ? "1b" : "1.1";
	case 12: return "1.2";
	case 13: return "1.3";
	case 20: return "2";
	case 21: return "2.1";
	case 22: return "2.2";
	case 30: return "3";
	case 31: return "3.1";
	case 32: return "3.2";
	case 40: return "4";
	case 41: return "4.1";
	case 42: return "4.2";
	case 50: return "5";
	case 51: return "5.1";
	case 52: return "5.2";
	default: return "UNKNOWN";
	}
}

float h264::getFrameRate() const
{
	if (vui_parameters_present_flag) {
		if (vui.timing_info_present_flag)
		{
			if (frame_mbs_only_flag)
			{
				return (float)vui.time_scale / (float)vui.num_units_in_tick / (float)2.0;
			}
			else
			{
				return (float)vui.time_scale / (float)vui.num_units_in_tick / (float)2.0;
			}
		}
	}
	return 0.0;
}

