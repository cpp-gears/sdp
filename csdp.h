#pragma once
#include "rfc/rfc4566.h"
#include <deque>
#include <memory>

namespace lib {
	class csdp {
	public:
		csdp();
		ssize_t serialize(std::string& sdp);
		ssize_t unserialize(const std::string& sdp);
		void clear();
		csdp(const csdp&);
		csdp& operator = (const csdp&);
	public:
		sdp::v Version;
		sdp::o Origin;
		sdp::s SessionName;
		sdp::i SessionInformation;
		sdp::u Uri;
		sdp::p Phone;
		sdp::e EMail;
		std::deque<sdp::c> Connection;
		sdp::b BandWidthCT;
		sdp::b BandWidthAS;
		std::deque<sdp::t> Timing;
		sdp::r RepeatTimes;
		sdp::z TimeZones;
		sdp::k EncryptionKeys;
		sdp::a::control	Control;
		sdp::m Video, Audio, Application;

		std::map<std::string, sdp::a> Attributes;
		std::multimap<std::string, sdp::m> Media;
	};
}