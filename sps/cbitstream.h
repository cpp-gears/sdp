#pragma once
#include <cinttypes>
#include <cstdlib>


namespace lib {
	namespace sps {
		class cbitstream
		{
		public:
			cbitstream(const uint8_t* stream, std::size_t length) : sData{ stream }, sLength{ length << 3 }{; }
			~cbitstream() = default;
			inline __attribute__((always_inline)) unsigned int getBits() {
				unsigned int result = 0;
				if (sPosition < sLength) {
					result = (sData[(sPosition >> 3)] >> (8 - ((sPosition % 8) + 1))) & 0x01;
					++sPosition;
				}
				return result;
			}
			inline void skipBits(int n) {
				if ((sPosition + n) < sLength) sPosition += n;
				else sPosition = sLength - 1;
			}
			inline unsigned int getBits(int n) {
				unsigned int r = 0;
				for (int i = 0; i < n; i++) { r |= (getBits() << (n - i - 1)); }
				return r;
			}
			inline unsigned int getExponentialGolombCode() {
				unsigned int r = 0;
				int i = 0;
				while ((getBits() == 0) && (i < 32)) { i++; }
				r = getBits(i);
				r += (1 << i) - 1;
				return r;
			}
			inline unsigned int getUE() {
				return getExponentialGolombCode();
			}
			inline unsigned int getSE() {
				auto r = getExponentialGolombCode();
				return (r & 0x01) ? ((r + 1) >> 1) : (-(r >> 1));
			}
		private:
			const uint8_t* sData{ nullptr };
			std::size_t sLength{ 0 }, sPosition{ 0 };
		};
	}
}
