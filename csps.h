#pragma once
#include "isps.h"
#include "sps/h264sps.h"
#include "sps/h265sps.h"

namespace lib {
	template<typename SPS_T>
	class csps : public sps::isps {
	public:
		using sps_t = SPS_T;
	public:
		csps(const uint8_t* sps_data, size_t sps_length) : spsParam{ sps_data, sps_length } { ; }
		~csps() { ; }
		virtual inline const sps_t* operator -> () const { return &spsParam; }
		virtual inline int getWidth() const { return spsParam.getWidth(); }
		virtual inline int getHeight() const { return spsParam.getHeight(); }
		virtual inline float getFrameRate() const { return spsParam.getFrameRate(); }
		virtual inline int getFormat() const { return spsParam.getFormat(); }
		virtual inline const char* getCodecName() const { return spsParam.getCodecName(); }
		virtual inline const char* getProfileName() const { return spsParam.getProfileName(); }
		virtual inline uint8_t getProfileIdc() const { return spsParam.getProfileIdc(); }
		virtual inline const char* getColorFormatName() const { return spsParam.getColorFormatName(); }
		virtual inline const char* getLevelName() const { return spsParam.getLevelName(); }
		virtual inline uint8_t getLevelIdc() const { return spsParam.getLevelIdc(); }

		inline const sps_t& getSPS() { return spsParam; }
	private:
		sps_t	spsParam;
	};
}