#include "csdp.h"

using namespace lib;

ssize_t csdp::unserialize(const std::string& sdp) {
	std::string_view stream{ sdp };

	clear();

	if (stream.empty() || stream.front() != 'v') { return -EINVAL; }
	while (!stream.empty()) {
		switch (stream.front()) {
		case 'v': { stream >> Version; break;  }
		case 'o': { stream >> Origin; break;  }
		case 's': { stream >> SessionName; break;  }
		case 'i': { stream >> SessionInformation; break;  }
		case 'u': { stream >> Uri; break;  }
		case 'e': { stream >> EMail; break;  }
		case 'p': { stream >> Phone; break;  }
		case 'c': { Connection.push_back({}); stream >> Connection.back(); break; }
		case 'b': { sdp::b b; stream >> b; if (b.Type == "CT") { BandWidthCT = b; } else if (b.Type == "AS") { BandWidthAS = b; } break; }
		case 't': { Timing.push_back({}); stream >> Timing.back(); break; }
		case 'r': { stream >> RepeatTimes; break;  }
		case 'z': { stream >> TimeZones; break;  }
		case 'k': { stream >> EncryptionKeys; break;  }
		case 'a': { 
			sdp::a a; stream >> a; 
			if (a.Attribute != "control") {
				Attributes.emplace(a.Attribute, a);
			}
			else {
				a >> Control;
			}
			break; 
		}
		case 'm': { 
			sdp::m m; stream >> m; 
			if (m.Media == "video" && Video.empty()) {
				Video = m;
			}
			else if (m.Media == "audio" && Audio.empty()) {
				Audio = m;
			}
			else if (m.Media == "application" && Application.empty()) {
				Application = m;
			}
			else {
				Media.emplace(m.Media, m);
			}
			break;
		}
		case '\r': case '\n':
			stream.remove_suffix(1);
			break;
		default:
			return -EINVAL;
		}
	}
	return 0;
}

ssize_t csdp::serialize(std::string& s) {
	
	s << Version << Origin << SessionName << SessionInformation << Control << Uri << EMail << Phone <<
		BandWidthCT << BandWidthAS;
	for (auto&& v : Connection) { s << v; }
	for (auto&& v : Timing) { s << v; }
	s << RepeatTimes << TimeZones << EncryptionKeys;
	for (auto&& v : Attributes) { s << v.second; }
	s << Video << Audio << Application;
	for (auto&& v : Media) { s << v.second; }
	return 0;
}

csdp::csdp(const csdp& s) :
	Version{ s.Version }, Origin{ s.Origin }, SessionName{ s.SessionName }, 
	SessionInformation{ s.SessionInformation }, Uri{ s.Uri }, Phone{ s.Phone }, 
	EMail{ s.EMail }, Connection{ s.Connection }, BandWidthCT{ s.BandWidthCT },
	BandWidthAS{ s.BandWidthAS }, Timing{ s.Timing }, RepeatTimes{ s.RepeatTimes }, 
	TimeZones{ s.TimeZones }, EncryptionKeys{ s.EncryptionKeys }, Control{ s.Control }, 
	Video{ s.Video }, Audio{ s.Audio }, Application{ s.Application }, Attributes{ s.Attributes }, Media{ s.Media }
{
}

csdp& csdp::operator = (const csdp& s) {
	Version = s.Version;
	Origin = s.Origin;
	SessionName = s.SessionName;
	SessionInformation = s.SessionInformation;
	Uri = s.Uri;
	Phone = s.Phone;
	EMail = s.EMail;
	Connection = s.Connection;
	BandWidthCT = s.BandWidthCT;
	BandWidthAS = s.BandWidthAS;
	Timing = s.Timing;
	RepeatTimes = s.RepeatTimes;
	TimeZones = s.TimeZones;
	EncryptionKeys = s.EncryptionKeys;
	Control = s.Control;
	Video = s.Video;
	Audio = s.Audio;
	Application = s.Application;
	Attributes = s.Attributes;
	Media = s.Media;
	return *this;
}

void csdp::clear() {
	Version.clear();
	Origin.clear();
	SessionName.clear();
	SessionInformation.clear();
	Uri.clear();
	Phone.clear();
	EMail.clear();
	Connection.clear();
	BandWidthCT.clear();
	BandWidthAS.clear();
	Timing.clear();
	RepeatTimes.clear();
	TimeZones.clear();
	EncryptionKeys.clear();
	Control.clear();
	Video.clear();
	Audio.clear();
	Application.clear();
	Attributes.clear();
	Media.clear();
}

csdp::csdp() { ; }
