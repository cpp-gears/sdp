#pragma once
#include <string>
#include <vector>
#include <unordered_map>
#include <map>
#include <ctime>

#define sdpattrid(ch) static inline const char _c{ ch }

namespace lib {
	namespace sdp {
		struct attr { virtual ~attr() { ; } };
		/* Session description */
		// v=  (protocol version)
		struct v : attr { 
			std::string No; sdpattrid('v'); 
			inline bool empty() const { return No.empty(); }
			inline void clear() { No.clear(); }
		};
		// o=  (originator and session identifier)
		struct o : attr { 
			std::string Owner, SessId, SessVersion, NetType, AddrType, Address;  sdpattrid('o'); 
			inline bool empty() const { return Owner.empty() && SessId.empty(); }
			inline void clear() { Owner.clear(); SessId.clear(); SessVersion.clear(); NetType.clear(); AddrType.clear(); Address.clear(); }
		};
		// s=  (session name)
		struct s : attr { 
			std::string Name; sdpattrid('s'); 
			inline bool empty() const { return Name.empty(); }
			inline void clear() { Name.clear(); }
		};
		// i=* (session information)
		struct i : attr { 
			std::string Information;  sdpattrid('i');
			inline bool empty() const { return Information.empty(); }
			inline void clear() { Information.clear(); }
		};
		// u=* (URI of description)
		struct u : attr { 
			std::string Address;  sdpattrid('u');
			inline bool empty() const { return Address.empty(); }
			inline void clear() { Address.clear(); }
		};
		// e=* (RFC 2822 email address)
		struct e : attr { 
			std::string Address;  sdpattrid('e');
			inline bool empty() const { return Address.empty(); }
			inline void clear() { Address.clear(); }
		};
		// p=* (phone number)
		struct p : attr { 
			std::string Number;  sdpattrid('p');
			inline bool empty() const { return Number.empty(); }
			inline void clear() { Number.clear(); }
		};
		// c=* (connection information -- not required if included in all media)
		struct c : attr { 
			std::string NetType, AddrType, Address, TTL, NumberOfAddresses;  sdpattrid('c'); 
			inline bool empty() const { return NetType.empty() && AddrType.empty() && Address.empty(); }
			inline void clear() { NetType.clear(); AddrType.clear(); Address.clear(); TTL.clear(); NumberOfAddresses.clear(); }
		};
		// b=* (zero or more bandwidth information lines)
		struct b : attr { 
			std::string Type, Bandwidth;  sdpattrid('b'); 
			inline bool empty() const { return Bandwidth.empty(); }
			inline void clear() { Type.clear(); Bandwidth.clear(); }
		};
		// t=  (time the session is active)
		// To convert these values to UNIX time, subtract decimal 2208988800.
		struct t : attr {
			std::string Start, Stop;  sdpattrid('t'); 
			t() = default;
			t(std::time_t start, std::time_t stop) : Start{ std::to_string(start + 2208988800) }, Stop{ std::to_string(start + 2208988800) } { ; }
			inline bool empty() const { return Start.empty() && Stop.empty(); }
			inline void clear() { Start.clear(); Stop.clear(); }
		};
		// r=* (zero or more repeat times)
		// To convert these values to UNIX time, subtract decimal 2208988800.
		struct r : attr { 
			std::string RepeatInterval, ActiveDuration, OffsetsFrom, StartTime;  sdpattrid('r'); 
			inline bool empty() const { return RepeatInterval.empty(); }
			inline void clear() { RepeatInterval.clear(); ActiveDuration.clear(); OffsetsFrom.clear(); StartTime.clear(); }
		};
		// z=* (time zone adjustments)
		struct z : attr { 
			struct tz { std::string Time, Offset; }; 
			std::vector<tz> Zone;  sdpattrid('z'); 
			inline bool empty() const { return Zone.empty(); } 
			inline void clear() { Zone.clear(); }
		};
		// k=* (encryption key)
		struct k : attr { 
			std::string Method, Key;  sdpattrid('k'); 
			inline bool empty() const { return Method.empty(); } 
			inline void clear() { Method.clear(); Key.clear(); }
		};
		// a=* (zero or more session attribute lines)
		struct a : attr {
			// a=control:<*> | <absolute url (rtsp://)> | <relative url>
			struct control { 
				std::string Uri; 
				inline bool empty() const { return Uri.empty(); }
				inline void clear() { Uri.clear(); }
			};
			// a=rtpmap:<payload type> <encoding name>/<clock rate>[/<encoding parameters > ]
			struct rtpmap { 
				std::string PayloadType, EncodingName, ClockRate, EncodingParameters; 
				inline bool empty() const { return PayloadType.empty(); }
				inline void clear() { PayloadType.clear(); EncodingName.clear(); ClockRate.clear(); EncodingParameters.clear(); }
			};
			// a=fmtp:<format> <format specific parameters>
			struct fmtp { 
				std::string Format; std::map<std::string, std::string> Parameters;
				inline bool empty() const { return Format.empty(); }
				inline void clear() { Format.clear(); Parameters.clear(); }
				inline std::string parameter(const std::string& name) const { if (auto&& it{ Parameters.find(name) }; it != Parameters.end()) { return it->second; } return {  }; }
				bool sps(std::vector<uint8_t>& sps_data);
				bool pps(std::vector<uint8_t>& pps_data);
				bool vps(std::vector<uint8_t>& vps_data);
			};

			std::string Attribute, Value;
			
			inline bool empty() const { return Attribute.empty() && Value.empty(); }
			inline void clear() { Attribute.clear(); Value.clear(); }

			sdpattrid('a');
		};
		// m=  (media name and transport address)
		struct m : attr {
			sdpattrid('m');
			std::string Media, Port, NumberOfPorts, Proto, Format; a::control Control; a::rtpmap Rtpmap; a::fmtp Fmtp; 
			sdp::i	SessionInformation;
			sdp::b	BandWidth;
			sdp::k	EncryptionKeys;
			sdp::c	Connection;
			std::unordered_map<std::string, sdp::a> Attributes; 
			inline bool get(const std::string& attr, sdp::a& v) const { if (auto&& it = Attributes.find(attr); it != Attributes.end()) { v = it->second; return true; } return false; }
			inline bool empty() const { return Media.empty(); }
			inline void clear() { Media.clear(); Port.clear(); NumberOfPorts.clear(); Proto.clear(); Format.clear(); Control.clear(); Rtpmap.clear(); Fmtp.clear(); 
				SessionInformation.clear(); BandWidth.clear(); EncryptionKeys.clear(); Connection.clear(); }
		};

		std::string_view& operator >> (std::string_view& sdp, sdp::v& v);
		std::string& operator << (std::string& sdp, sdp::v& v);
		std::string_view& operator >> (std::string_view& sdp, sdp::o& v);
		std::string& operator << (std::string& sdp, sdp::o& v);
		std::string_view& operator >> (std::string_view& sdp, sdp::s& v);
		std::string& operator << (std::string& sdp, sdp::s& v);
		std::string_view& operator >> (std::string_view& sdp, sdp::i& v);
		std::string& operator << (std::string& sdp, sdp::i& v);
		std::string_view& operator >> (std::string_view& sdp, sdp::e& v);
		std::string& operator << (std::string& sdp, sdp::e& v);
		std::string_view& operator >> (std::string_view& sdp, sdp::p& v);
		std::string& operator << (std::string& sdp, sdp::p& v);
		std::string_view& operator >> (std::string_view& sdp, sdp::u& v);
		std::string& operator << (std::string& sdp, sdp::u& v);
		std::string_view& operator >> (std::string_view& sdp, sdp::c& v);
		std::string& operator << (std::string& sdp, sdp::c& v);
		std::string_view& operator >> (std::string_view& sdp, sdp::b& v);
		std::string& operator << (std::string& sdp, sdp::b& v);
		std::string_view& operator >> (std::string_view& sdp, sdp::t& v);
		std::string& operator << (std::string& sdp, const sdp::t& v);
		std::string_view& operator >> (std::string_view& sdp, sdp::r& v);
		std::string& operator << (std::string& sdp, sdp::r& v);
		std::string_view& operator >> (std::string_view& sdp, sdp::z& v);
		std::string& operator << (std::string& sdp, sdp::z& v);
		std::string_view& operator >> (std::string_view& sdp, sdp::k& v);
		std::string& operator << (std::string& sdp, sdp::k& v);
		std::string_view& operator >> (std::string_view& sdp, sdp::a& v);
		std::string& operator << (std::string& sdp, sdp::a& v);
		std::string_view& operator >> (std::string_view& sdp, sdp::m& v);
		std::string& operator << (std::string& sdp, sdp::m& v);

		void operator >> (sdp::a& sdp, sdp::a::control& v);
		std::string& operator << (std::string& sdp, sdp::a::control& v);

		void operator >> (sdp::a& sdp, sdp::a::rtpmap& v);
		std::string& operator << (std::string& sdp, sdp::a::rtpmap& v);

		void operator >> (sdp::a& sdp, sdp::a::fmtp& v);
		std::string& operator << (std::string& sdp, sdp::a::fmtp& v);
	}
}