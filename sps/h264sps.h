#pragma once
#include "../isps.h"
#include <cinttypes>
#include <cstdlib>

namespace lib {
	namespace sps {
		class h264 : public sps::isps {
		public:
			int frame_crop_left_offset{ 0 };
			int frame_crop_right_offset{ 0 };
			int frame_crop_top_offset{ 0 };
			int frame_crop_bottom_offset{ 0 };
			int profile_idc{ 0 };
			int constraint_set0_flag{ 0 };
			int constraint_set1_flag{ 0 };
			int constraint_set2_flag{ 0 };
			int constraint_set3_flag{ 0 };
			int constraint_set4_flag{ 0 };
			int constraint_set5_flag{ 0 };
			int reserved_zero_2bits{ 0 };
			int level_idc{ 0 };
			int seq_parameter_set_id{ 0 };
			int chroma_format_idc{ 0 };
			int residual_colour_transform_flag{ 0 };
			int bit_depth_luma_minus8{ 0 };
			int bit_depth_chroma_minus8{ 0 };
			int qpprime_y_zero_transform_bypass_flag{ 0 };
			int seq_scaling_matrix_present_flag{ 0 };
			int seq_scaling_list_present_flag{ 0 };
			int delta_scale{ 0 };
			int log2_max_frame_num_minus4{ 0 };
			int pic_order_cnt_type{ 0 };
			int log2_max_pic_order_cnt_lsb_minus4{ 0 };
			int delta_pic_order_always_zero_flag{ 0 };
			int offset_for_non_ref_pic{ 0 };
			int offset_for_top_to_bottom_field{ 0 };
			int num_ref_frames_in_pic_order_cnt_cycle{ 0 };
			int max_num_ref_frames{ 0 };
			int gaps_in_frame_num_value_allowed_flag{ 0 };
			int pic_width_in_mbs_minus1{ 0 };
			int pic_height_in_map_units_minus1{ 0 };
			int frame_mbs_only_flag{ 0 };
			int mb_adaptive_frame_field_flag{ 0 };
			int direct_8x8_inference_flag{ 0 };
			int frame_cropping_flag{ 0 };
			int vui_parameters_present_flag{ 0 };

			struct {
				int aspect_ratio_info_present_flag{ 0 }; //0 u(1) 
				int aspect_ratio_idc{ 0 };               //0 u(8) 
				int sar_width{ 0 };                      //0 u(16) 
				int sar_height{ 0 };                     //0 u(16) 
				int overscan_info_present_flag{ 0 };     //0 u(1) 
				int overscan_appropriate_flag{ 0 };      //0 u(1) 
				int video_signal_type_present_flag{ 0 }; //0 u(1) 
				int video_format{ 0 };                   //0 u(3) 
				int video_full_range_flag{ 0 };          //0 u(1) 
				int colour_description_present_flag{ 0 }; //0 u(1) 
				int colour_primaries{ 0 };                //0 u(8) 
				int transfer_characteristics{ 0 };        //0 u(8) 
				int matrix_coefficients{ 0 };             //0 u(8) 
				int chroma_loc_info_present_flag{ 0 };     //0 u(1) 
				int chroma_sample_loc_type_top_field{ 0 };  //0 ue(v) 
				int chroma_sample_loc_type_bottom_field{ 0 }; //0 ue(v) 
				int timing_info_present_flag{ 0 };          //0 u(1) 
				int num_units_in_tick{ 0 };           //0 u(32) 
				int time_scale{ 0 };                 //0 u(32) 
				int fixed_frame_rate_flag{ 0 };           //0 u(1) 
				int nal_hrd_parameters_present_flag{ 0 }; //0 u(1)
				int cpb_cnt_minus1{ 0 };                 //0 ue(v)
				int bit_rate_scale{ 0 };                 //0 u(4)
				int cpb_size_scale{ 0 };                 //0 u(4)
				int bit_rate_value_minus1[16]{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, };      //0 ue(v)
				int cpb_size_value_minus1[16]{ 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, };      //0 ue(v)
				int cbr_flag[16]{ 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, };                   //0 u(1)
				int initial_cpb_removal_delay_length_minus1{ 0 }; //0 u(5)
				int cpb_removal_delay_length_minus1{ 0 };         //0 u(5)
				int dpb_output_delay_length_minus1{ 0 };         //0 u(5)
				int time_offset_length{ 0 };                      //0 u(5)
				int vcl_hrd_parameters_present_flag{ 0 };         //0 u(1)
				int low_delay_hrd_flag{ 0 };                      //0 u(1)
				int pic_struct_present_flag{ 0 };//0 u(1)
				int bitstream_restriction_flag{ 0 };              //0 u(1)
				int motion_vectors_over_pic_boundaries_flag{ 0 };  //0 ue(v)
				int max_bytes_per_pic_denom{ 0 };                  //0 ue(v)
				int max_bits_per_mb_denom{ 0 };                    //0 ue(v)
				int log2_max_mv_length_horizontal{ 0 };            //0 ue(v)
				int log2_max_mv_length_vertical{ 0 };              //0 ue(v)
				int num_reorder_frames{ 0 };                       //0 ue(v)
				int max_dec_frame_buffering{ 0 };                  //0 ue(v)
			} vui;
		public:
			h264(const uint8_t* sps_data, size_t sps_length);
			~h264() = default;

			virtual int getWidth() const;
			virtual int getHeight() const;
			virtual float getFrameRate() const;
			virtual int getFormat() const { return frame_mbs_only_flag; }
			virtual inline const char* getCodecName() const { return "H264"; }
			virtual const char* getProfileName() const;
			virtual inline uint8_t getProfileIdc() const { return (uint8_t)(profile_idc); }
			virtual const char* getColorFormatName() const;
			virtual const char* getLevelName() const;
			virtual inline uint8_t getLevelIdc() const { return (uint8_t)(level_idc); }
		};
	}
}