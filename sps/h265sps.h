#pragma once
#include "../isps.h"
#include <cinttypes>
#include <cstdlib>
#include <vector>

namespace lib {
	namespace sps {
		class h265 : public sps::isps {
		public:
			uint8_t spsVideoParameterSetId{ 0 };
			uint8_t spsMaxSubLayersMinus1{ 0 };
			uint8_t spsTemporalIdNestingFlag{ 0 };
			uint8_t generalProfileSpace{ 0 };
			uint8_t generalTierFlag{ 0 };
			uint8_t generalProfileIdc{ 0 };
			uint8_t generalProfileCompatibilityFlag[32] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
			uint8_t generalProgressiveSourceFlag{ 0 };
			uint8_t generalInterlacedSourceFlag{ 0 };
			uint8_t generalNonPackedConstraintFlag{ 0 };
			uint8_t generalFrameOnlyConstraintFlag{ 0 };
			uint8_t generalLevelIdc{ 0 };

			std::vector<uint8_t> subLayerProfilePresentFlag;
			std::vector<uint8_t> subLayerLevelPresentFlag;
			std::vector<uint8_t> subLayerProfileSpace;
			std::vector<uint8_t> subLayerTierFlag;
			std::vector<uint8_t> subLayerProfileIdc;
			std::vector<std::vector<uint8_t>> subLayerProfileCompatibilityFlag;
			std::vector<uint8_t> subLayerProgressiveSourceFlag;
			std::vector<uint8_t> subLayerInterlacedSourceFlag;
			std::vector<uint8_t> subLayerNonPackedConstraintFlag;
			std::vector<uint8_t> subLayerFrameOnlyConstraintFlag;
			std::vector<uint8_t> subLayerLevelIdc;

			uint32_t spsSeqParameterSetId{ 0 };
			uint32_t chromaFormatIdc{ 0 };
			uint8_t separateColourPlaneFlag{ 0 };
			uint32_t picWidthInLumaSamples{ 0 };
			uint32_t picHeightInLumaSamples{ 0 };
			uint8_t conformanceWindowFlag{ 0 };
			uint32_t confWinLeftOffset{ 0 };
			uint32_t confWinRightOffset{ 0 };
			uint32_t confWinTopOffset{ 0 };
			uint32_t confWinBottomOffset{ 0 };
			uint32_t bitDepthLumaMinus8{ 0 };
			uint32_t bitDepthChromaMinus8{ 0 };
		public:
			h265(const uint8_t* sps_data, size_t sps_length);
			~h265() = default;

			virtual int getWidth() const;
			virtual int getHeight() const;
			virtual float getFrameRate() const;
			virtual int getFormat() const { return 0; }
			virtual inline const char* getCodecName() const { return "H265"; }
			virtual const char* getProfileName() const;
			virtual inline uint8_t getProfileIdc() const { return (uint8_t)(generalLevelIdc); }
			virtual const char* getColorFormatName() const;
			virtual const char* getLevelName() const;
			virtual inline uint8_t getLevelIdc() const { return (uint8_t)(generalLevelIdc); }
		};
	}
}