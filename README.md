# Session Description Protocol (SDP) library
---

## Using the SDP class
---
```cpp
std::string sdpSource{
    "v=0\n"
    "o=- 2251938193 2251938193 IN IP4 0.0.0.0\n"
    "s=RTSP Session/2.0\n"
    "c=IN IP4 0.0.0.0\n"
    "e=Jane Doe <j.doe@example.com>\n"
    "p=+1 617 555-6011\n"
    "u=uri://\n"
    "i=description\n"
    "t=0 0\n"
    "a=control:*\n"
    "a=range:npt=now-\n"
    "a=packetization-supported:DH\n"
    "m=video 0 RTP/AVP 96\n"
    "a=control:trackID=0\n"
    "a=rtpmap:96 H264/90000\n"
    "a=fmtp:96 packetization-mode=1;profile-level-id=4D0020;sprop-parameter-sets=Z00AIJpkAoA8/4C1AQEBQAAA+gAAMNQ6GAAQR4AAEEeC7y40MAAgjwAAII8F3lwo,aO48gA==\n"
    "a=recvonly\n"
    "m=audio 0 RTP/AVP 0\n"
    "c=IN IP4 0.0.0.0\n"
    "b=AS:50\n"
    "a=recvonly\n"
    "a=control:rtsp://10.7.167.50/Streaming/Channels/101/trackID=2\n"
    "a=rtpmap:0 PCMU/8000/1\n"
    "a=Media_header:MEDIAINFO=494D4B48010300000400000110710110401F000000FA000000000000000000000000000000000000\n"
    "a=appversion:1.0\n"
    "m=application 0 RTP/AVP 107\n"
    "a=control:trackID=1\n"
    "a=rtpmap:107 vnd.onvif.metadata/90000\n"
    "a=recvonly\n"
};
    
lib::csdp sdp;
sdp.unserialize(sdpSource);
    
if (sdp.Version.No == "0") {
    if (!sdp.Video.empty()) {
        printf("VIDEO( %-3s ): %s\n", sdp.Video.Format.c_str(), sdp.Video.Rtpmap.EncodingName.c_str());
    }
    if (!sdp.Audio.empty()) {
        printf("AUDIO( %-3s ): %s\n", sdp.Audio.Format.c_str(), sdp.Audio.Rtpmap.EncodingName.c_str());
    }
    if (!sdp.Application.empty()) {
        printf("APP  ( %-3s ): %s\n", sdp.Application.Format.c_str(), sdp.Application.Rtpmap.EncodingName.c_str());
    }
}
```