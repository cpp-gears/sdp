#include "rfc4566.h"
#include <deque>
#include <algorithm>

using namespace lib;

static constexpr const struct { const char* pt; const char* name; const char* media;  const char* max_channels, *clock_rate; float frame_size; size_t default_packet_size; } rfc4856PayloadTypes[] = {
	{ "0", "PCMU", "audio",		"1", "8000", 0, 20 },
	{ "1", "reserved", "audio", "1", "8000", 0, 0 },
	{ "2", "reserved", "audio", "1", "8000", 0, 0 },
	{ "3", "GSM", "audio",		"1", "8000", 20, 20 },
	{ "4", "G723", "audio",		"1", "8000", 30, 30 },
	{ "5", "DVI4", "audio",		"1", "8000", 0, 20 },
	{ "6", "DVI4", "audio",		"1", "16000", 0, 20 },
	{ "7", "LPC", "audio",		"1", "8000", 0, 20 },
	{ "8", "PCMA", "audio",		"1", "8000", 0, 20 },
	{ "9", "G722", "audio",		"1", "8000", 0, 20 },
	{ "10", "L16", "audio",		"2", "44100", 0, 20 },
	{ "11", "L16", "audio",		"1", "44100", 0, 20 },
	{ "12", "QCELP", "audio",	"1", "8000", 20, 20 },
	{ "13", "CN", "audio",		"1", "8000", 0, 0 },
	{ "14", "MPA", "audio",		"2", "90000", 72,  },
	{ "15", "G728", "audio",	"1", "8000", 2.5, 20 },
	{ "16", "DVI4", "audio",	"1", "11025", 0, 20 },
	{ "17", "DVI4", "audio",	"1", "22050", 0, 20 },
	{ "18", "G729", "audio",	"1", "8000", 10, 20 },
	{ "19", "reserved", "",		"0", "0", 0, 0 },
	{ "20", "reserved", "",		"0", "0", 0, 0 },
	{ "21", "reserved", "",		"0", "0", 0, 0 },
	{ "22", "reserved", "",		"0", "0", 0, 0 },
	{ "23", "reserved", "",		"0", "0", 0, 0 },
	{ "24", "reserved", "",		"0", "0", 0, 0 },
	{ "25", "CELB", "video",	"0", "90000", 0, 0 },
	{ "26", "JPEG", "video",	"0", "90000", 0,  },
	{ "27", "reserved", "",		"0", "0", 0, 0 },
	{ "28", "NV", "video",		"0", "90000", 0, 0 },
	{ "29", "reserved", "",		"0", "0", 0, 0 },
	{ "30", "reserved", "",		"0", "0", 0, 0 },
	{ "31", "H261", "video",	"0", "90000", 0, 0 },
	{ "32", "MPV", "video",		"0", "90000", 0, 0 },
	{ "33", "MP2T", "audio/video", "0", "90000", 0, 0 },
	{ "34", "H263", "video",	"0", "90000", 0, 0 },
};


static inline constexpr uint8_t sdpBase64TableDecode[] = {
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,		//   0..15
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,		//  16..31
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,  62, 255, 255, 255,  63,		//  32..47
	 52,  53,  54,  55,  56,  57,  58,  59,  60,  61, 255, 255, 255, 254, 255, 255,		//  48..63
	255,   0,   1,   2,   3,   4,   5,   6,   7,   8,   9,  10,  11,  12,  13,  14,		//  64..79
	 15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25, 255, 255, 255, 255, 255,		//  80..95
	255,  26,  27,  28,  29,  30,  31,  32,  33,  34,  35,  36,  37,  38,  39,  40,		//  96..111
	 41,  42,  43,  44,  45,  46,  47,  48,  49,  50,  51, 255, 255, 255, 255, 255,		// 112..127
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,		// 128..143
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
};

static std::vector<uint8_t> sdpBase64Decode(const uint8_t* source, std::size_t length) {
	std::vector<uint8_t> result;
	if (length) {
		result.reserve(length);
		auto in = source;
		for (auto len = length & (~3); len; len -= 4, in += 4) {
			result.push_back((uint8_t)(sdpBase64TableDecode[in[0]] << 2 | sdpBase64TableDecode[in[1]] >> 4));
			result.push_back((uint8_t)(sdpBase64TableDecode[in[1]] << 4 | sdpBase64TableDecode[in[2]] >> 2));
			result.push_back((uint8_t)(((sdpBase64TableDecode[in[2]] << 6) & 0xc0) | sdpBase64TableDecode[in[3]]));
		}
		while (length-- && source[length] == '=') { result.pop_back(); }
		result.shrink_to_fit();
	}
	return result;
}

static inline void strupper(std::string& val) {
	std::for_each(val.begin(), val.end(), [](char& c) {
		c = (char)std::toupper(c);
	});
}

static inline std::string_view strtrim(std::string_view& val) {
	while (!val.empty() && std::isspace(val.back())) { val.remove_suffix(1); }
	while (!val.empty() && std::isspace(val.front())) { val.remove_prefix(1); }
	return val;
}

static inline std::string_view strval(std::string_view& src) {
	std::string_view val;
	if (auto npos = src.find_first_of('\n'); npos != std::string_view::npos) {
		val = src.substr(0, npos);
		src.remove_prefix(npos + 1);
	}
	else {
		val = src.substr(0);
		src.remove_prefix(src.length());
	}
	return strtrim(val);
}


static inline std::deque<std::string_view> split(std::string_view& src, char separator,ssize_t n=-1) {
	char needle[3]{ separator, '\n', '\0' };
	std::deque<std::string_view> list;
	for (auto npos = src.find_first_of(needle); n; npos = src.find_first_of(needle), n--) {
		if (npos != std::string_view::npos) {
			list.emplace_back(src.substr(0, npos));
			strtrim(list.back());
			src.remove_prefix(npos);
			if (src.front() != '\n') { src.remove_prefix(1); continue; }
			src.remove_prefix(1);
			return list;
		}
		break;
	}
	if (!n) {
		if (auto npos = src.find_first_of('\n'); npos != std::string_view::npos) {
			list.emplace_back(src.substr(0, npos));
			strtrim(list.back());
			src.remove_prefix(npos);
		}
		else {
			if (!src.empty()) {
				list.emplace_back(src);
				strtrim(list.back());
				src.remove_prefix(src.length());
			}
		}
	}
	if (!src.empty()) {
		if (auto npos = src.find_first_of('\n'); npos != std::string_view::npos) {
			list.emplace_back(src.substr(0, npos));
			strtrim(list.back());
			src.remove_prefix(npos + 1);
		}
		else {
			list.emplace_back(src);
			strtrim(list.back());
			src.remove_prefix(src.length());
		}
	}
	return list;
}

std::string_view& sdp::operator >> (std::string_view& sdp, sdp::m& v) {
	/* m=<media> <port> <proto> <fmt> ... */
	if (sdp.front() == 'm') {
		sdp.remove_prefix(2);
		auto&& params{ split(sdp,' ',3) };
		
		switch (auto&& port{ split(params[1],'/',1) }; port.size()) {
		case 2:
			v.NumberOfPorts = port[1];
		case 1:
			v.Port = port[0];
		}

		v.Media = params[0];
		v.Proto = params[2];
		v.Format = params[3];

		bool foundRtpMap{ false }, DoIt{true};

		while (DoIt && !sdp.empty() && sdp.front() != 'm') {
			switch (sdp.front()) {
			case 'a':
			{
				sdp::a a;
				sdp >> a;
				if (a.Attribute == "control") {
					a >> v.Control;
				}
				else if (a.Attribute == "rtpmap") {
					foundRtpMap = true;
					a >> v.Rtpmap;
					strupper(v.Rtpmap.EncodingName);
				}
				else if (a.Attribute == "fmtp") {
					a >> v.Fmtp;
				}
				else {
					v.Attributes.emplace(a.Attribute, a);
				}
				break;
			}
			case 'i':
				sdp >> v.SessionInformation;
				break;
			case 'b':
				sdp >> v.BandWidth;
				break;
			case 'k':
				sdp >> v.EncryptionKeys;
				break;
			case 'c':
				sdp >> v.Connection;
				break;
			default:
				DoIt = false;
				break;
			}
		}
		if (!foundRtpMap) {
			if (size_t nFormat{ std::stoul(v.Format) }; nFormat < 35) {
				v.Rtpmap.PayloadType = rfc4856PayloadTypes[nFormat].pt;
				v.Rtpmap.EncodingName = rfc4856PayloadTypes[nFormat].name;
				v.Rtpmap.ClockRate = rfc4856PayloadTypes[nFormat].clock_rate;
				v.Rtpmap.EncodingParameters = rfc4856PayloadTypes[nFormat].max_channels;
			}
		}
	}
	return sdp;
}

std::string& sdp::operator << (std::string& sdp, sdp::m& v) {
	if (!v.Media.empty()) {
		sdp.append("m=").append(v.Media).append(" ").append(v.Port.empty() ? "0" : v.Port);
		if (!v.NumberOfPorts.empty()) {
			sdp.append("/").append(v.NumberOfPorts);
		}
		sdp.append(" ").append(v.Proto).append(" ").append(v.Format).push_back('\n');
		sdp << v.Control << v.Rtpmap << v.Fmtp;
		for (auto&& a : v.Attributes) {
			sdp.append("a=").append(a.second.Attribute);
			if (!a.second.Value.empty()) { sdp.append(":").append(a.second.Value); }
			sdp.push_back('\n');
		}
	}
	return sdp;
}

void sdp::operator >> (sdp::a& a, sdp::a::rtpmap& v) {
	// a=rtpmap:<payload type> <encoding name>/<clock rate>[/<encoding parameters > ]
	std::string_view val{ a.Value };
	auto&& params{ split(val,' ',1) };
	auto&& args{ split(params[1],'/',2) };
	v.PayloadType = params[0];
	switch (args.size()) {
	case 3:
		v.EncodingParameters = args[2];
	case 2:
		v.ClockRate = args[1];
	case 1:
		v.EncodingName = args[0];
	}
}

std::string& sdp::operator << (std::string& sdp, sdp::a::rtpmap& v) {
	sdp.append("a=rtpmap:").append(v.PayloadType).append(" ").append(v.EncodingName).append("/").append(v.ClockRate);
	if (!v.EncodingParameters.empty()) { sdp.append("/").append(v.EncodingParameters); }
	sdp.push_back('\n');
	return sdp;
}

bool sdp::a::fmtp::sps(std::vector<uint8_t>& sps_data) {

	if (auto&& sparams = Parameters.find("sprop-parameter-sets"); sparams != Parameters.end()) {
		std::string_view value{ sparams->second };
		switch (auto&& params{ split(value,',',1) }; params.size()) {
		case 2: case 1:
			sps_data = std::move(sdpBase64Decode((uint8_t*)params[0].data(),params[0].length()));
			return true;
		}
	}
	else if (auto&& sparams = Parameters.find("sprop-sps"); sparams != Parameters.end()) {
		sps_data = std::move(sdpBase64Decode((uint8_t*)sparams->second.data(), sparams->second.length()));
		return true;
	}

	return false;
}

bool sdp::a::fmtp::pps(std::vector<uint8_t>& pps_data) {
	if (auto&& sparams = Parameters.find("sprop-parameter-sets"); sparams != Parameters.end()) {
		std::string_view value{ sparams->second };
		switch (auto&& params{ split(value,',',1) }; params.size()) {
		case 2:
			pps_data = std::move(sdpBase64Decode((uint8_t*)params[1].data(), params[1].length()));
			return true;
		}
	}
	else if (auto&& sparams = Parameters.find("sprop-pps"); sparams != Parameters.end()) {
		pps_data = std::move(sdpBase64Decode((uint8_t*)sparams->second.data(), sparams->second.length()));
		return true;
	}

	return false;
}

bool sdp::a::fmtp::vps(std::vector<uint8_t>& vps_data) {
	if (auto&& sparams = Parameters.find("sprop-vps"); sparams != Parameters.end()) {
		vps_data = std::move(sdpBase64Decode((uint8_t*)sparams->second.data(), sparams->second.length()));
		return true;
	}

	return false;
}

void sdp::operator >> (sdp::a& a, sdp::a::fmtp& v) {
	// a=fmtp:<format> <format specific parameters>
	std::string_view val{ a.Value };
	auto&& params{ split(val,' ',1) };
	auto&& args{ split(params[1],';') };
	v.Format = params[0];

	for (auto&& ar : args) {
		auto&& kv{ split(ar, '=', 1) };
		v.Parameters.emplace(strtrim(kv[0]), kv[1]);
	}
}

std::string& sdp::operator << (std::string& sdp, sdp::a::fmtp& v) {
	if (!v.Format.empty()) {
		sdp.append("a=fmtp:").append(v.Format).append(" ");
		for (auto&& [k, v] : v.Parameters) {
			sdp.append(k).append("=").append(v).push_back(';');
		}
		sdp.pop_back();
		sdp.push_back('\n');
	}
	return sdp;
}

void sdp::operator >> (sdp::a& a, sdp::a::control& v) {
	v.Uri = a.Value;
}

std::string& sdp::operator << (std::string& sdp, sdp::a::control& v) {
	if (!v.Uri.empty()) {
		sdp.append("a=control:").append(v.Uri).push_back('\n');
	}
	return sdp;
}

std::string_view& sdp::operator >> (std::string_view& sdp, sdp::a& v) {
	/*
		a=<attribute>
		a=<attribute>:<value>
	*/
	if (sdp.front() == 'a') {
		sdp.remove_prefix(2);
		auto&& params{ split(sdp,':',1) };
		v.Attribute.assign(params[0]);
		if(params.size() > 1) {
			v.Value.assign(params[1]);
		}
	}
	return sdp;
}

std::string& sdp::operator << (std::string& sdp, sdp::a& v) {
	if (!v.Attribute.empty()) {
		sdp.append("a=").append(v.Attribute);
		if (!v.Value.empty()) {
			sdp.append(" ").append(v.Value);
		}
		sdp.push_back('\n');
	}
	return sdp;
}

std::string_view& sdp::operator >> (std::string_view& sdp, sdp::k& v) {
	/* 
	  k=<method>
      k=<method>:<encryption key>
	*/
	if (sdp.front() == 'k') {
		sdp.remove_prefix(2);
		auto&& params{ split(sdp,':',1) };
		v.Method.assign(params[0]);
		v.Key.assign(params[1]);
	}
	return sdp;
}

std::string& sdp::operator << (std::string& sdp, sdp::k& v) {
	if (!v.Method.empty()) {
		sdp.append("k=").append(v.Method);
		if (!v.Key.empty()) {
			sdp.append(" ").append(v.Key);
		}
		sdp.push_back('\n');
	}
	return sdp;
}

std::string_view& sdp::operator >> (std::string_view& sdp, sdp::z& v) {
	/*  z=<adjustment time> <offset> <adjustment time> <offset> ....  */
	if (sdp.front() == 'z') {
		sdp.remove_prefix(2);
		auto&& params{ split(sdp,' ') };
		v.Zone.reserve((params.size() + 1) >> 1);
		for (size_t i = 0; i < params.size(); i += 2) {
			v.Zone.emplace_back(sdp::z::tz{ std::string{params[i]}, std::string{params[i + 1]} });
		}
	}
	return sdp;
}

std::string& sdp::operator << (std::string& sdp, sdp::z& v) {
	if (!v.Zone.empty()) {
		sdp.append("z=");
		for (auto&& z : v.Zone) {
			sdp.append(z.Time).append(" ").append(z.Offset).push_back(' ');
		}
		sdp.pop_back();
		sdp.push_back('\n');
	}
	return sdp;
}

std::string_view& sdp::operator >> (std::string_view& sdp, sdp::r& v) {
	/* r=<repeat interval> <active duration> <offsets from start-time> */
	if (sdp.front() == 'r') {
		sdp.remove_prefix(2);
		auto&& params{ split(sdp,' ') };
		v.RepeatInterval.assign(params[0]);
		v.ActiveDuration.assign(params[1]);
		v.OffsetsFrom.assign(params[2]);
		v.StartTime.assign(params[3]);
	}
	return sdp;
}

std::string& sdp::operator << (std::string& sdp, sdp::r& v) {
	if (!(v.RepeatInterval.empty() && v.ActiveDuration.empty() && v.OffsetsFrom.empty() && v.StartTime.empty())) {
		sdp.append("r=").append(v.RepeatInterval).append(" ").append(v.ActiveDuration).append(" ").append(v.OffsetsFrom).append(" ").append(v.StartTime).push_back('\n');
	}
	return sdp;
}


std::string_view& sdp::operator >> (std::string_view& sdp, sdp::t& v) {
	/* t=<start-time> <stop-time> */
	if (sdp.front() == 't') {
		sdp.remove_prefix(2);
		auto&& params{ split(sdp,' ') };
		v.Start.assign(params[0]);
		v.Stop.assign(params[1]);
	}
	return sdp;
}

std::string& sdp::operator << (std::string& sdp, const sdp::t& v) {
	if (!(v.Start.empty() && v.Stop.empty())) {
		sdp.append("t=").append(v.Start).append(" ").append(v.Stop).push_back('\n');
	}
	return sdp;
}

std::string_view& sdp::operator >> (std::string_view& sdp, sdp::b& v) {
	/*  b=<bwtype>:<bandwidth> */
	if (sdp.front() == 'b') {
		sdp.remove_prefix(2);
		auto&& params{ split(sdp,':') };
		v.Type.assign(params[0]);
		v.Bandwidth.assign(params[1]);
	}
	return sdp;
}

std::string& sdp::operator << (std::string& sdp, sdp::b& v) {
	if (!(v.Type.empty() && v.Bandwidth.empty())) {
		sdp.append("b=").append(v.Type).append(":").append(v.Bandwidth).push_back('\n');
	}
	return sdp;
}

std::string_view& sdp::operator >> (std::string_view& sdp, sdp::c& v) {
	/* c=<nettype> <addrtype> <connection-address>=<base multicast address>[/<ttl>]/<number of addresses> */
	if (sdp.front() == 'c') {
		sdp.remove_prefix(2);
		auto&& params{ split(sdp,' ') };
		switch (params.size()) {
		case 3:
		{
			auto&& addr{ split(params[2],'/') };
			v.Address.assign(addr[0]);
			if (addr.size() > 2) {
				v.NumberOfAddresses.assign(addr[2]);
			}
			if (addr.size() > 1) {
				v.TTL.assign(addr[1]);
			}
		}
		case 2:
			v.AddrType.assign(params[1]);
		case 1:
			v.NetType.assign(params[0]);
			break;
		}
	}
	return sdp;
}

std::string& sdp::operator << (std::string& sdp, sdp::c& v) {
	sdp.append("c=");
	sdp.append(v.NetType.empty() ? "IN" : v.NetType).push_back(' ');
	sdp.append(v.AddrType.empty() ? "IP4" : v.AddrType).push_back(' ');
	sdp.append(v.Address.empty() ? "0.0.0.0" : v.Address);
	if (!v.TTL.empty()) { sdp.append("/").append(v.TTL); }
	if (!v.NumberOfAddresses.empty()) { sdp.append("/").append(v.NumberOfAddresses); }
	sdp.push_back('\n');
	return sdp;
}

std::string_view& sdp::operator >> (std::string_view& sdp, sdp::o& v) {
	/* o=<username> <sess-id> <sess-version> <nettype> <addrtype> <unicast-address> */
	if (sdp.front() == 'o') {
		sdp.remove_prefix(2);
		auto&& params{ split(sdp,' ')};
		
		v.Owner.assign(params[0]);
		v.SessId.assign(params[1]);
		v.SessVersion.assign(params[2]);
		v.NetType.assign(params[3]);
		v.AddrType.assign(params[4]);
		v.Address.assign(params[5]);
	}
	return sdp;
}

std::string& sdp::operator << (std::string& sdp, sdp::o& v) {
	sdp.append("o=");
	sdp.append(v.Owner.empty() ? "-" : v.Owner).push_back(' ');
	sdp.append(v.SessId).push_back(' ');
	sdp.append(v.SessVersion).push_back(' ');
	sdp.append(v.NetType.empty() ? "IN" : v.NetType).push_back(' ');
	sdp.append(v.AddrType.empty() ? "IP4" : v.AddrType).push_back(' ');
	sdp.append(v.Address.empty() ? "0.0.0.0" : v.Address).push_back('\n');
	return sdp;
}

std::string_view& sdp::operator >> (std::string_view& sdp, sdp::u& v) {
	if (sdp.front() == 'u') {
		sdp.remove_prefix(2);
		v.Address.assign(strval(sdp));
	}
	return sdp;
}

std::string& sdp::operator << (std::string& sdp, sdp::u& v) {
	if (!v.Address.empty()) {
		sdp.append("u=").append(v.Address).push_back('\n');
	}
	return sdp;
}

std::string_view& sdp::operator >> (std::string_view& sdp, sdp::p& v) {
	if (sdp.front() == 'p') {
		sdp.remove_prefix(2);
		v.Number.assign(strval(sdp));
	}
	return sdp;
}

std::string& sdp::operator << (std::string& sdp, sdp::p& v) {
	if (!v.Number.empty()) {
		sdp.append("p=").append(v.Number).push_back('\n');
	}
	return sdp;
}

std::string_view& sdp::operator >> (std::string_view& sdp, sdp::e& v) {
	if (sdp.front() == 'e') {
		sdp.remove_prefix(2);
		v.Address.assign(strval(sdp));
	}
	return sdp;
}

std::string& sdp::operator << (std::string& sdp, sdp::e& v) {
	if (!v.Address.empty()) {
		sdp.append("e=").append(v.Address).push_back('\n');
	}
	return sdp;
}

std::string_view& sdp::operator >> (std::string_view& sdp, sdp::i& v) {
	if (sdp.front() == 'i') {
		sdp.remove_prefix(2);
		v.Information.assign(strval(sdp));
	}
	return sdp;
}

std::string& sdp::operator << (std::string& sdp, sdp::i& v) {
	if (!v.Information.empty()) {
		sdp.append("i=").append(v.Information).push_back('\n');
	}
	return sdp;
}

std::string_view& sdp::operator >> (std::string_view& sdp, sdp::s& v) {
	if (sdp.front() == 's') {
		sdp.remove_prefix(2);
		v.Name.assign(strval(sdp));
	}
	return sdp;
}

std::string& sdp::operator << (std::string& sdp, sdp::s& v) {
	if (!v.Name.empty()) {
		sdp.append("s=").append(v.Name).push_back('\n');
	}
	return sdp;
}

std::string_view& sdp::operator >> (std::string_view& sdp, sdp::v& v) {
	if (sdp.front() == 'v') {
		sdp.remove_prefix(2);
		v.No.assign(strval(sdp));
	}
	return sdp;
}

std::string& sdp::operator << (std::string& sdp, sdp::v& v) {
	sdp.append("v=").append(v.No.empty() ? "0" : v.No).push_back('\n');
	return sdp;
}